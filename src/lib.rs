use regex::Regex;
use std::fs::{self, OpenOptions};
use std::io::{self, Write};
use std::path::Path;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum EditError {
    #[error("search pattern not found")]
    NotFound,
    #[error("expected one pattern match, found multiple")]
    MultipleMatches,
    #[error("bad pattern")]
    BadPattern,
    #[error("regex error")]
    BadRegex(#[source] regex::Error),
    #[error("error opening file")]
    ErrOpenFile(#[source] io::Error),
    #[error("error reading file")]
    ErrReadFile(#[source] io::Error),
    #[error("error writing file")]
    ErrWriteFile(#[source] io::Error),
}

#[derive(Debug)]
pub struct Editor<'a> {
    path: Option<&'a Path>,
    string: Option<&'a mut String>,
    delim_l: String,
    delim_r: String,
    indent: bool,
}

impl Editor<'_> {
    pub fn indent(mut self) -> Self {
        self.indent = true;
        self
    }
    pub fn delimiter(mut self, left: &str, right: &str) -> Self {
        self.delim_l = left.into();
        self.delim_r = right.into();
        self
    }
    pub fn replace_one(mut self, pattern: &str, replacement: &str) -> Result<Self, EditError> {
        if let Some(ref mut string) = self.string {
            replace_one(string, pattern, replacement, &self.delim_l, &self.delim_r)?;
            return Ok(self);
        }
        if let Some(path) = self.path {
            edit_file(path, |src: &mut String| {
                replace_one(src, pattern, replacement, &self.delim_l, &self.delim_r)
            })?;
            return Ok(self);
        }
        unreachable!()
    }
    pub fn replace_all(mut self, pattern: &str, replacement: &str) -> Result<Self, EditError> {
        if let Some(ref mut string) = self.string {
            replace_all(string, pattern, replacement, &self.delim_l, &self.delim_r)?;
            return Ok(self);
        }
        if let Some(path) = self.path {
            edit_file(path, |src: &mut String| {
                replace_all(src, &pattern, &replacement, &self.delim_l, &self.delim_r)
            })?;
            return Ok(self);
        }
        unreachable!()
    }
    pub fn comment_one(self, pattern: &str, comment: &str) -> Result<Self, EditError> {
        self.replace_one(pattern, &format!("{comment} $0"))
    }
    pub fn comment_all(self, pattern: &str, comment: &str) -> Result<Self, EditError> {
        self.replace_all(pattern, &format!("{comment} $0"))
    }
    pub fn insert_line_before(mut self, pattern: &str, line: &str) -> Result<Self, EditError> {
        if let Some(ref mut string) = self.string {
            insert_line(
                string,
                pattern,
                line,
                self.indent,
                false,
                &self.delim_l,
                &self.delim_r,
            )?;
            return Ok(self);
        }
        if let Some(path) = self.path {
            edit_file(path, |src: &mut String| {
                insert_line(
                    src,
                    pattern,
                    line,
                    self.indent,
                    false,
                    &self.delim_l,
                    &self.delim_r,
                )
            })?;
            return Ok(self);
        }
        unreachable!()
    }
    pub fn insert_line_after(mut self, pattern: &str, line: &str) -> Result<Self, EditError> {
        if let Some(ref mut string) = self.string {
            insert_line(
                string,
                pattern,
                line,
                self.indent,
                true,
                &self.delim_l,
                &self.delim_r,
            )?;
            return Ok(self);
        }
        if let Some(path) = self.path {
            edit_file(path, |src: &mut String| {
                insert_line(
                    src,
                    pattern,
                    line,
                    self.indent,
                    true,
                    &self.delim_l,
                    &self.delim_r,
                )
            })?;
            return Ok(self);
        }
        unreachable!()
    }
    pub fn remove_lines(mut self, pattern: &str) -> Result<Self, EditError> {
        if let Some(ref mut string) = self.string {
            remove_lines(string, pattern, &self.delim_l, &self.delim_r)?;
            return Ok(self);
        }
        if let Some(path) = self.path {
            edit_file(path, |src: &mut String| {
                remove_lines(src, pattern, &self.delim_l, &self.delim_r)
            })?;
            return Ok(self);
        }
        unreachable!()
    }
}

impl<'a> From<&'a Path> for Editor<'a> {
    fn from(path: &'a Path) -> Self {
        Self {
            delim_l: "<".into(),
            delim_r: ">".into(),
            path: Some(path),
            string: None,
            indent: false,
        }
    }
}

impl<'a> From<&'a mut String> for Editor<'a> {
    fn from(string: &'a mut String) -> Self {
        Self {
            delim_l: "<".into(),
            delim_r: ">".into(),
            string: Some(string),
            path: None,
            indent: false,
        }
    }
}

fn replace_one(
    src: &mut String,
    pattern: &str,
    replacement: &str,
    delim_l: &str,
    delim_r: &str,
) -> Result<(), EditError> {
    let count = replace_all(src, &pattern, &replacement, delim_l, delim_r)?;
    if count == 1 {
        Ok(())
    } else {
        Err(EditError::MultipleMatches)
    }
}

fn replace_all(
    src: &mut String,
    pattern: &str,
    replacement: &str,
    delim_l: &str,
    delim_r: &str,
) -> Result<usize, EditError> {
    let re = Regex::new(&escape_regex::interpolate(pattern, delim_l, delim_r))
        .map_err(EditError::BadRegex)?;
    let count = re.find_iter(src).collect::<Vec<_>>().len();
    *src = re.replace_all(src, replacement).to_string();
    Ok(count)
}

fn insert_line(
    src: &mut String,
    pattern: &str,
    line: &str,
    indent: bool,
    after: bool,
    delim_l: &str,
    delim_r: &str,
) -> Result<(), EditError> {
    let found = find_lines(src, pattern, delim_l, delim_r)?;
    if found.len() == 0 {
        return Err(EditError::NotFound);
    }
    // TODO:
    // if matched_blocks.len() > 1 {
    //     return Err(EditError::MultipleMatches);
    // }
    let mut res_lines = vec![];
    let lines: Vec<_> = src.lines().collect();
    for (n, src_line) in lines.into_iter().enumerate() {
        if found.contains(&n) {
            let line = if indent {
                let spaces = Regex::new(r"^(\s*).*$")
                    .unwrap()
                    .captures(src_line)
                    .unwrap()
                    .get(1)
                    .unwrap()
                    .as_str()
                    .to_string();
                spaces + line
            } else {
                line.to_string()
            };
            if after {
                res_lines.push(src_line.to_owned());
                res_lines.push(line);
            } else {
                res_lines.push(line);
                res_lines.push(src_line.to_owned());
            }
        } else {
            res_lines.push(src_line.to_owned());
        }
    }
    *src = res_lines.join("\n");
    return Ok(());
}

fn remove_lines(
    src: &mut String,
    pattern: &str,
    delim_l: &str,
    delim_r: &str,
) -> Result<(), EditError> {
    let patterns: Vec<&str> = pattern.trim().lines().collect(); // TODO: optional trim
    if patterns.len() == 0 {
        return Err(EditError::BadPattern);
    }

    let lines: Vec<&str> = src.lines().collect();
    let mut matched_blocks = vec![];
    for first_match in find_lines(src, patterns[0], delim_l, delim_r)? {
        let mut all_matched = true;
        for (n, pattern) in patterns[1..].iter().enumerate() {
            if !match_line(lines[first_match + 1 + n], pattern, delim_l, delim_r)? {
                all_matched = false;
                break;
            }
        }
        if all_matched {
            matched_blocks.push(first_match);
        }
    }
    if matched_blocks.len() == 0 {
        return Err(EditError::NotFound);
    }
    // TODO:
    // if matched_blocks.len() > 1 {
    //     return Err(EditError::MultipleMatches);
    // }
    let mut res_lines = vec![];
    for (n, line) in lines.into_iter().enumerate() {
        let mut push = true;
        for block in matched_blocks.iter() {
            if n >= *block && n < block + patterns.len() {
                push = false;
                break;
            }
        }
        if push {
            res_lines.push(line);
        }
    }
    *src = res_lines.join("\n");
    return Ok(());
}

fn find_lines(
    src: &String,
    pattern: &str,
    delim_l: &str,
    delim_r: &str,
) -> Result<Vec<usize>, EditError> {
    let mut lines = vec![];
    for (n, line) in src.lines().enumerate() {
        if match_line(line, pattern, delim_l, delim_r)? {
            lines.push(n);
        }
    }
    Ok(lines)
}

fn match_line(src: &str, pattern: &str, delim_l: &str, delim_r: &str) -> Result<bool, EditError> {
    Ok(
        Regex::new(&escape_regex::interpolate(pattern, delim_l, delim_r))
            .map_err(EditError::BadRegex)?
            .is_match(src),
    )
}

fn edit_file<F, R>(path: &Path, edit: F) -> Result<R, EditError>
where
    F: Fn(&mut String) -> Result<R, EditError>,
{
    let mut src = fs::read_to_string(path).map_err(EditError::ErrReadFile)?;
    let res = edit(&mut src)?;
    let mut file = OpenOptions::new()
        .write(true)
        .truncate(true)
        .open(path)
        .map_err(EditError::ErrOpenFile)?;
    file.write(src.as_bytes())
        .map_err(EditError::ErrWriteFile)?;
    Ok(res)
}

#[cfg(test)]
mod tests {
    use indoc::indoc;
    use std::fs;
    use std::io::Write;
    use tempfile::NamedTempFile;

    #[test]
    fn test_file_replace_one() {
        let mut file = NamedTempFile::new().unwrap();
        write!(file, "Hello World!").unwrap();

        let path = file.path();
        super::Editor::from(path)
            .replace_one("Hello", "Hi")
            .unwrap();

        let text = fs::read_to_string(path).unwrap();
        assert_eq!(text, "Hi World!");
    }
    #[test]
    fn test_file_replace_one_regex() {
        let mut file = NamedTempFile::new().unwrap();
        write!(file, "Hello World!").unwrap();

        let path = file.path();
        super::Editor::from(path)
            .replace_one(r"H<\w{3}>o", "Hi")
            .unwrap();

        let text = fs::read_to_string(path).unwrap();
        assert_eq!(text, "Hi World!");
    }
    #[test]
    fn test_insert_line_before() {
        let mut text = String::from("1\n2\n3");
        super::Editor::from(&mut text)
            .insert_line_before("1", "+")
            .unwrap();
        assert_eq!(text, "+\n1\n2\n3");
    }
    #[test]
    fn test_insert_line_after() {
        let mut text = String::from("1\n2\n3");
        super::Editor::from(&mut text)
            .insert_line_after("3", "+")
            .unwrap();
        assert_eq!(text, "1\n2\n3\n+");
    }
    #[test]
    fn test_remove_line() {
        let mut text = String::from("1\n2\n3");
        super::Editor::from(&mut text).remove_lines("2").unwrap();
        assert_eq!(text, "1\n3");
    }
    #[test]
    fn test_remove_lines() {
        let mut text = String::from("1\n2\n3\n4\n5");
        super::Editor::from(&mut text)
            .remove_lines("2\n3\n4")
            .unwrap();
        assert_eq!(text, "1\n5");
    }

    #[test]
    fn test_comment_yaml() {
        let text = indoc! {"
          test_1:
          test_a:
          test_2:
        "};
        let mut text = text.to_string();
        let expected = indoc! {"
          # test_1:
          test_a:
          # test_2:
        "};
        super::Editor::from(&mut text)
            .comment_all(r"test_<\d>:", "#")
            .unwrap();
        assert_eq!(text, expected);
    }

    #[test]
    fn test_remove_lines_in_yaml() {
        let text = indoc! {"
          test_1:
            block_1:
              - a
              - b
              - c
              - d
              - e
            block_2:
              - a
              - b
              - c
              - d
              - e
          test_2:
            block_1:
              - a
              - b
              - c
              - d
              - e
            block_2:
              - a
              - b
              - c
              - d
              - e
          test_3:
            nest:
              block_1:
                - a
                - b
                - c
                - d
                - e
              block_2:
                - a
                - b
                - c
                - d
                - e
        "};
        let mut text = text.to_string();
        let pattern = indoc! {"
            block_1:
              - a
              - b
              - <[c]>
              - d
              - e
        "};
        super::Editor::from(&mut text)
            .remove_lines(pattern)
            .unwrap();
        let expected = indoc! {"
          test_1:
            block_2:
              - a
              - b
              - c
              - d
              - e
          test_2:
            block_2:
              - a
              - b
              - c
              - d
              - e
          test_3:
            nest:
              block_2:
                - a
                - b
                - c
                - d
                - e
        "};
        let expected = expected.trim();
        assert_eq!(text, expected);
    }
}
